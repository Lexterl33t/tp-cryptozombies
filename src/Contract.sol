// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;
import "./ownable.sol";

contract ZombieFactory is Ownable {
    
    uint dnaDigits = 16;
    uint dnaModulus =  10 ** dnaDigits;
    uint cooldownTime = 1 days;

    Zombie[] public zombies;

    mapping (uint => address) public zombieToOwner;
    mapping (address => uint) public ownerZombieCount;

    struct Zombie {
        string name;
        uint dna;
        uint32 level;
        uint32 readyTime;
    }

    event NewZombie(uint zombieId, string name, uint dna);

    function _createZombie(string memory _name, uint _dna) internal {

        Zombie memory zombie = Zombie(_name, _dna, 1, uint32(block.timestamp + cooldownTime));

        zombies.push(zombie);
        uint zombieId = zombies.length - 1;

        zombieToOwner[zombieId] = msg.sender;
        ownerZombieCount[msg.sender]++;

        emit NewZombie(zombieId, _name, _dna);
    }

    function _generateRandomDna(string memory _str) private view returns (uint) {
        uint rand = uint(keccak256(abi.encodePacked(_str)));
        return rand % dnaModulus;
    }

    function createRandomZombie(string memory _name) public {
        require(ownerZombieCount[msg.sender] == 0);

        uint randDna = _generateRandomDna(_name);
        _createZombie(_name,randDna);
    }

    

}

