pragma solidity ^0.8.13;

import "./Contract.sol";

interface KittyInterface {
    function getKitty(uint256 _id) external view returns (
        bool isGestating,
        bool isReady,
        uint256 cooldownIndex,
        uint256 nextActionAt,
        uint256 siringWithId,
        uint256 birthTime,
        uint256 matronId,
        uint256 sireId,
        uint256 generation,
        uint256 genes
    );
}

contract ZombieFeeding is ZombieFactory{

    KittyInterface KittyContract;

    function _triggerCooldown(Zombie storage _zombie) internal {
        _zombie.readyTime = uint32(block.timestamp + cooldownTime);

    } 

    function _isReady(Zombie storage _zombie) internal view returns (bool) {
        if (_zombie.readyTime <= block.timestamp) {
            return true;
        }

        return false;
    }
 
    function feedAndMultiply(uint _zombieId, uint _targetDna, string memory _species) internal {
        require(zombieToOwner[_zombieId] == msg.sender);
        Zombie storage zombie = zombies[_zombieId];
        require(_isReady(zombie));

        _targetDna = _targetDna % dnaModulus;

        uint newDna = (_targetDna + zombie.dna) / 2;

        if (keccak256(abi.encodePacked(_species)) == keccak256("kitty")) {
            newDna = newDna - newDna % 100 + 99;
        }


        _createZombie("NoName", newDna);
        _triggerCooldown(zombie);
    }

    function setKittyContractAddress(address _address) external onlyOwner {
        KittyContract = KittyInterface(_address);
    }

    function feedOnKitty(uint _zombieId, uint _kittyId) public {
        (,,,,,,,,,uint kittyDna) = KittyContract.getKitty(_kittyId);

        feedAndMultiply(_zombieId, kittyDna, "kitty");
    }

    
}
